def heygodistance = Math.round(doc['geohash'].distance(lat,lon) )

if(heygodistance < 1000){
        heygodistance = 999
}else if(heygodistance >1000 && heygodistance < 3850){
        heygodistance = 3850
}else if(heygodistance >=3850 && heygodistance < 8750){
        heygodistance = 8750
}else if(heygodistance >=8750 && heygodistance < 15660){
        heygodistance = 15660
}else if(heygodistance >=15660 && heygodistance < 33160){
        heygodistance = 33160
}else{
	heygodistance = 72220
}
return heygodistance
